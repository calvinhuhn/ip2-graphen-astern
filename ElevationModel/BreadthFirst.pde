public class BreathFirst implements SearchAlgorithm{
      /**Breitensuche.
     * @param start Start-Knoten
     * @param ziel End-Knoten
     * @return Laenge des kuerzesten Pfades von start nach ziel
     */
    public void search(Vertex start, Vertex ziel) {
          //println("Weg von Start in " + start.toString() + " zu " + ziel.toString() + " wird gesucht");
          Queue<Vertex> toBeVisited = new LinkedList<Vertex>();
          Set<Vertex> visited = new TreeSet<Vertex>();
  
          toBeVisited.add(start);
          //println("start visited");
          while (!toBeVisited.isEmpty()) {
            //println(toBeVisited.size());
            //println("neuer Node wird angeschaut");
                  Vertex node = toBeVisited.poll();
                  if (node.equals(ziel)) {
                          markPath(start, ziel);
                          return;
                  }
                  visited.add(node);
                  Set<Vertex> neighbors = getNeighbors(node);
                 // println(neighbors.toString());
                  //println(visited.toString());
                  for (Vertex neighbor : neighbors){
                    //println(visited.contains(neighbor));
                          if (!visited.contains(neighbor)) {
                             neighbor.nextInSearchPath = node;
                                  toBeVisited.add(neighbor);
                                  //println("Neue Nachbarn hinzugefügt");
                          }
                  }
          }
   println("Kein Weg gefunden :(");
          
  }
    

  private Set<Vertex> getNeighbors(Vertex node) {
    return node.getNextVertecies();
  }
  
  public void markPath(Vertex start, Vertex end){
    Vertex currentNode = end;
    if (start != null && end != null){
       while (currentNode != start){
         //println("Find next");
        currentNode.inSearchPath = true;
        if (currentNode.nextInSearchPath != null){
            currentNode = currentNode.nextInSearchPath;
        } else {

          break;
        }
    } 
              end.inSearchPath = true;
          start.inSearchPath = true;
    }
    
   
  }
  
  
  
  /*
  BFS(start_node, goal_node) {
 for(all nodes i) visited[i] = false; // anfangs sind keine Knoten besucht
 queue.push(start_node);              // mit Start-Knoten beginnen
 visited[start_node] = true;
 while(! queue.empty() ) {            // solange queue nicht leer ist
  node = queue.pop();                 // erstes Element von der queue nehmen
  if(node == goal_node) {
   return true;                       // testen, ob Ziel-Knoten gefunden
  }
  foreach(child in expand(node)) {    // alle Nachfolge-Knoten, …
   if(visited[child] == false) {      // … die noch nicht besucht wurden …
    queue.push(child);                // … zur queue hinzufügen…
    visited[child] = true;            // … und als bereits gesehen markieren
   }
  }
 }
 return false;                        // Knoten kann nicht erreicht werden
}
*/
}
