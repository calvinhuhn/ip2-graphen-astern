public class AStar extends Graph implements SearchAlgorithm{
  
  public AStar() {
    super();
  }
  
  public void search(Vertex start, Vertex end) {
    println("AStar started");
    aStar(start, end);
  }
  
  public void reconstruct_path(HashMap<Vertex, Vertex> cameFrom, Vertex goal){
    
    if(goal != null) {
      
      goal.nextInSearchPath = cameFrom.get(goal);
      goal.inSearchPath = true;
      reconstruct_path(cameFrom, cameFrom.get(goal));
    }
    
    
  }

  public void aStar(Vertex start, Vertex goal){

    // The set of nodes already evaluated
    ArrayList<Vertex> closedSet = new ArrayList<Vertex>();

    // The set of currently discovered nodes that are not evaluated yet.
    // Initially, only the start node is known.
    ArrayList<Vertex> openSet = new ArrayList<Vertex>();
    openSet.add(start);

    // For each node, which node it can most efficiently be reached from.
    // If a node can be reached from many nodes, cameFrom will eventually contain the
    // most efficient previous step.
    // cameFrom := an empty map
    HashMap<Vertex, Vertex> cameFrom = new HashMap<Vertex, Vertex>();
    cameFrom.put(start, null);

    // For each node, the cost of getting from the start node to that node.
    // gScore = map with default value of Infinity
    HashMap<Vertex, Float> gScore = new HashMap<Vertex, Float>();

    // The cost of going from start to start is zero.
    // gScore[start] = 0
    gScore.put(start, 0.0);

    // For each node, the total cost of getting from the start node to the goal
    // by passing by that node. That value is partly known, partly heuristic.
    // fScore := map with default value of Infinity
    HashMap<Vertex, Float> fScore = new HashMap<Vertex, Float>();
    
    // For the first node, that value is completely heuristic.
    fScore.put(start, heuristic_cost_estimate(start, goal));

    while(openSet.size() != 0) {
        Vertex current = lowestScore(openSet, fScore);
        
        if(current == goal) {
            break;
        }

        openSet.remove(current);
        closedSet.add(current);

        for(Vertex neighbor : current.getNextVertecies()) {
            if(closedSet.contains(neighbor)) {
                continue;   // Ignore the neighbor which is already evaluated.
            }
            // The distance from start to a neighbor
            float tentative_gScore = gScore.get(current) + dist_between(current, neighbor);

            if(!openSet.contains(neighbor)) {  // Discover a new node
                openSet.add(neighbor);
            } else if(tentative_gScore >= gScore.get(neighbor)) {
                continue;   // This is not a better path.
            }
            // This path is the best until now. Record it!
            cameFrom.put(neighbor, current);
            gScore.put(neighbor, tentative_gScore);
            fScore.put(neighbor, gScore.get(neighbor) + heuristic_cost_estimate(neighbor, goal));
            
        }
     }
     reconstruct_path(cameFrom, goal);
  }
  
  public float dist_between(Vertex one, Vertex two) {
    if (one.position.z < waterlevel){
      return 10000;
    }
    return (one.edges.get(two) / 10);
    //return pow((one.edges.get(two) / 10), 30);
  }
  
  public float heuristic_cost_estimate(Vertex one, Vertex two) {
    return Math.abs(one.getPosition().x - two.getPosition().x) + Math.abs(one.getPosition().y - two.getPosition().y);
    //return Math.sqrt(Math.pow(one.getPosition().x - two.getPosition().x, 2) + Math.pow(one.getPosition().y - two.getPosition().y, 2));
  }
  
  public Vertex lowestScore(ArrayList<Vertex> set, HashMap<Vertex, Float> score) {
    float value = Integer.MAX_VALUE;
    Vertex node = null;
    for(Vertex v : set) {
      if(score.get(v) < value) {
        value = score.get(v);
        node = v;
      }
    }
    return node;
  }
}
