import java.io.*;
Map elevationMap;
Graphics engine;
public float angleZ = 0;
public float angleX = 0;
public float waterlevel = 40;

  float x = 0;
  float z = 0;
  float y = 0;
  float scroll = 10;

void setup() {
  
  elevationMap = new Map();
  engine = new Graphics(elevationMap, waterlevel);
  //size(1000, 1000, P3D);
  pixelDensity(2);
  surface.setResizable(true);
  colorMode(HSB, 360, 100, 100);
  fullScreen(P3D);
  ortho(-width/2, width/2, -height/2, height/2);

}
void draw() {
  translate(width/2 + x, height/2 + y, z );    //Drehung erfolgt von linker seite, daher muss mittig zentriert werden
  //engine.drawVertecies();
  engine.render();
  if (keyPressed){
    if (key == 'w'){
      z = z + scroll;
    } else if(key == 's'){
      z = z - scroll;
    }
    if (key == 'a'){
      x = x + scroll;
    } else if(key == 'd'){
      x = x - scroll;
    }
    
     if (key == 'e'){
      y = y + scroll;
    } else if(key == 'q'){
      y = y - scroll;
    }
  } 
}

void mouseDragged() {
    if((angleX + (pmouseY - mouseY) * 0.01 > 0 ) && (angleX + (pmouseY - mouseY) * 0.01 < 1.4 ) )
    angleX += (pmouseY - mouseY) * 0.01;
    angleZ += (pmouseX - mouseX) * 0.006;
}

void fileSelected(File selection) throws Exception{
  if (selection == null) {
    println("Window was closed or the user hit cancel. No File selected.");
  } else {
    FilterReader filter = null;
    BufferedReader buffered = null;
    FileReader reader = null;
    ArrayList<Float> heightData = new ArrayList<Float>();
    char[] c = new char[100];
    String filename = selection.getAbsolutePath();
    try {
      reader = new FileReader(filename);
      buffered = new BufferedReader(reader);
      filter = new Filter(buffered);
      int i = 0;
      do {
        i = filter.read(c, 0, c.length);
        String s = "";
        if(i > 0) {
          for (int j = 0; j < i; j++) {
            s += c[j];
          }
          if (s != "") {
            heightData.add(Float.valueOf(s));
          }
        }
      } while (i != -1);
    } catch(Exception e) {
      e.printStackTrace();
    } finally {
      if(reader != null) {
        reader.close();
      }
      if(buffered != null) {
        buffered.close();
      }
      if(filter != null) {
        filter.close();
      }
    }
    //for(float f:heightData) {
    //  println(f);
    //}
    elevationMap.input(heightData);
    println("In der Datei befinden sich " + heightData.size() + " Zahlen. Die Map muss also " + (int)Math.sqrt(heightData.size()) + " x " + (int)Math.sqrt(heightData.size()) + " Felder groß sein.");
  }
}

void mouseMoved() {
  //engine.selectPoint();
}

void keyReleased() {
   if (key == '1') {
     engine.heightview = engine.heightview ? false : true;
   }
  if (key == ' '){
    angleZ = 0;
    angleX = 0;
    x = 0;
    z = 0;
    y = 0;
  }
  if(key=='b' || key == 'v'){
    thread("search");
  }
  if (key == 'r' && !engine.selectionView){
    elevationMap = new Map(engine);
    engine.changeMap(elevationMap);
  }
  if (key == 'n'){
    selectInput("Wähle eine Datei aus", "fileSelected");
  }
  if (key == 'o'){
    angleZ = 0;
    angleX = 0;
    x = 0;
    z = 0;
    y = 0;
    engine.changeRenderMode();
  }
  if (key == 'r' && engine.selectionView){
    elevationMap.selectedStart = null;
    elevationMap.selectedEnd= null;
  }
}

public void mousePressed() {
  if (engine.selectionView){
      engine.selectPoint("select");
  }
}

public synchronized void search() {
  engine.search(null, null, key);
}
