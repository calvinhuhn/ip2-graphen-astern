import java.io.FilterReader;
import java.io.IOException;
import java.io.Reader;

public class Filter extends FilterReader {

  public Filter(Reader in) {
    super(in);
  }
  
  public int read (char cbuf[], int off, int len) throws IOException{
    int character = in.read();
    if(character == -1) {
      return -1;
    } else {
      int pos = 0;
      while(character != ',' && character != -1) {
        cbuf[off + pos] = (char)character;
        pos++;
        character = in.read();
      }
      return pos;
    }
  }
}
