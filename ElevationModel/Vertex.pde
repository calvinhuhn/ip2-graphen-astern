import java.util.*; 
class Vertex implements Comparable <Vertex>{
  public HashMap<Vertex, Float> edges;
  public PVector position = new PVector(0, 0, 0);
  
  public boolean inSearchPath = false;
  public Vertex nextInSearchPath = null;
  
  public Vertex() {
    edges = new HashMap<Vertex, Float>();
    position.x = 300;
    position.y = random(0, 100);
    position.z = 300;
    
  }
  
    public Vertex(int x, int y) {
      edges = new HashMap<Vertex, Float>();
      position.x = x;
      position.y = y;
      position.z = random(0, 50);
    }
  
    public Vertex (int x, int y, float z) {
      edges = new HashMap<Vertex, Float>();
      position = new PVector(x, y, z);
    }
    /*
    @Override
    public int compareTo(Vertex v){
          return this.edges.size() - v.edges.size();
    }
    */
    
    @Override
    public int compareTo(Vertex v){
          return (int)((this.position.x * width + this.position.y) - (v.position.x * width + v.position.y));
    }
    
    @Override
    public String toString(){
        return "vertex: Position("+ position + ")\n";
    }
    /**
    * Calculates the Weight 
    */
    public int getWeight(Vertex secondVertex) {
      return (int)abs(this.position.z - secondVertex.position.z);
        
    }
    
    /**
    * Creates a edge between the current Vertex to a second given Vertex. 
    * The edges weight is determined by the height difference
    */
    public void addEdge(Vertex secondVertex, Float weight){
      edges.put(secondVertex, weight);
    }
    
    public HashMap<Vertex, Float> getEdges(){
      return edges;
    }

    public Set<Vertex> getNextVertecies() {
      return edges.keySet();
    }
    
    public PVector getPosition() {
        return position;
    }
    
    

  
}
