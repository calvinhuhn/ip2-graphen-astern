import java.util.PriorityQueue;
class Graph{
  
  public int dimensions = 100; //Creates a 10x10 field
  public int marginVertical = 20;
  float minHeight = 0;
  float maxHeight = 1;
  float peak = 100;//100 Standart
  public float terrainFactor = 15;
  public float terrain = dimensions * 0.01 * terrainFactor;
  
  public int gridSize = (height - ( marginVertical * 2)) / (dimensions);  //
  public int marginHorizontal = (width - (dimensions * gridSize)) / 2;
  public PriorityQueue<Vertex> vertices;
  
  Vertex selectedStart = null;
  Vertex selectedEnd = null;
  
  public void init() {
     maxHeight = 200;  //100 Standart
     float terrainFactor = 15;
     float terrain = dimensions * 0.01 * terrainFactor;
  
      int gridSize = (height - ( marginVertical * 2)) / (dimensions);  //
      int marginHorizontal = (width - (dimensions * gridSize)) / 2;
    
  }
  
  public Graph() {
    println("Graph generated");
    vertices = new PriorityQueue<Vertex>(1);
  }

  public Vertex maxDegreeVertex() {
    return vertices.peek();
  }
  
  public void input(ArrayList<Float> input ){
    vertices = new PriorityQueue<Vertex>();
    println(dimensions);
    dimensions = (int)sqrt(input.size()) ;
    init();
    println(dimensions);
    int x = 1;
    int y = 1;
    minHeight = Integer.MAX_VALUE;
    maxHeight = Integer.MIN_VALUE;
    for (float currentHeight : input) {
      if(currentHeight < minHeight)
        minHeight = currentHeight;
      if(currentHeight > maxHeight)
        maxHeight = currentHeight;
    }
    for (float currentHeight: input){
      //println("Vertex added x: " + x + " y: " + y);
      vertices.add(new Vertex(x * gridSize + marginHorizontal, y * gridSize + marginVertical, map(currentHeight, minHeight, maxHeight, 0, peak)));
      if(y < dimensions){
        y++;
      } else {
        y = 1;
        x++;
      }
    } 
    println("start");
    generatesEdges();
  
  }
  

  /**
  *Creates a Sample Graph with some random Gaps
  */
  public void createSampleGraph() {
    for (int x = 1; x <= dimensions; x++){
      for (int y = 1; y <= dimensions; y++) {
        float _x = x;
        float _y = y;
        noiseSeed(frameCount);
        float vertexHeight = map(noise(_x / (dimensions / terrain), _y / (dimensions /terrain)), 0, 1, 0, peak);
        vertices.add(new Vertex(x * gridSize + marginHorizontal, y * gridSize + marginVertical, vertexHeight));
      }
    }
    generatesEdges();
  }
  public void generatesEdges() {
     Vertex[] verticesArray = new Vertex[0];
    verticesArray = vertices.toArray(verticesArray);
   try {
   for (int v = 0; v < verticesArray.length; v++){
     int positionX = v / (dimensions );
     int positionY = v % (dimensions );
     //println(positionX + "|" + positionY);
     
     //Sucht position in 1D Array 
     //Beispiel: 100x100 Feld
     //positionX = 99 position Y = 99
     //99 * 100 + 99 = 9999
     if((positionX ) * dimensions + positionY  < verticesArray.length  &&(positionX + 2 <= dimensions )){
       //Random
       //horizontal
         createEdge(verticesArray[v], verticesArray[(positionX + 1) * dimensions + positionY]);
       
     }
     
      if(((positionX * dimensions) + positionY < verticesArray.length)&&(positionY + 1 < dimensions )){
        
      //Verical
           createEdge(verticesArray[v], verticesArray[positionX * dimensions + positionY+ 1 ]);
       
      }
   }
   } catch (Exception e){}
  }
  public float calcEdge(Vertex a, Vertex b){
    float heightEdge = abs(a.position.z - b.position.z);
    float widthEdge = sqrt(abs(pow(a.position.x - b.position.x, 2) - pow(a.position.y - b.position.y, 2)));
    float hypotenuse = sqrt(pow(heightEdge, 2) + pow(widthEdge, 2));
  
    return hypotenuse;
  }
  /**
  * Creates an Edge between two Vertecies
  */
  public void createEdge(Vertex a, Vertex b){
    Float weight = calcEdge(a, b);
    a.addEdge(b, weight);
    b.addEdge(a, weight);
  
  }
  
  

}
