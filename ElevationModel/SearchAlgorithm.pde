public interface SearchAlgorithm {
  public void search(Vertex start, Vertex end);
}
