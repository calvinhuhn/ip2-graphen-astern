import java.util.Iterator;
public class Graphics {
  color gray = color(200, 200, 200);
  color background = color(20);
  color highlighting = color (200, 0, 0);
  float waterlevel = 40;
  boolean heightview = true;
  
  boolean selectionView = false;
  
  Map myMap;
  public Graphics(Map map, float water){
    myMap = map;
    colorMode(HSB, 360, 100, 100);
    waterlevel = water;
  }
  
  public void changeMap(Map map) {
    myMap = map;
  }
  
  public void render() {
    try {
      rotateX(angleX); //60 Grad drehung   
      rotateZ(angleZ);
      translate(-width /2, -height /2);
      background(0);
      PriorityQueue<Vertex> vericesToBeVisited = new PriorityQueue<Vertex>();
      for ( Vertex current : myMap.getVertices()){
          vericesToBeVisited.add(current);
      }
      fill(0, 255, 0);
      beginShape(LINES);
      while(vericesToBeVisited.size() > 0){
        Vertex current = vericesToBeVisited.poll();
        for (Vertex neighbor : current.getNextVertecies()){
             
          if (vericesToBeVisited.contains(neighbor)){
            if (heightview) {
              stroke(color(map(((current.position.z + neighbor.position.z) / 2), 4, myMap.peak, 120, 0), 100, 100));
            } else {
              stroke(color(map(current.edges.get(neighbor), 4, 25, 120, 0), 100, 100));
            }
              strokeWeight(1);
              if(neighbor.inSearchPath && current.inSearchPath && !selectionView){
                strokeJoin(ROUND);
                strokeWeight(7);
              }
              
              if (!selectionView){
                
                //WATERlevel 
                if(current.position.z< waterlevel && neighbor.position.z < waterlevel){
                  stroke(color(220, 77, 84));
                  vertex(current.position.x, current.position.y, waterlevel);
                  vertex(neighbor.position.x, neighbor.position.y, waterlevel);
                
                } else if(current.position.z< waterlevel || neighbor.position.z < waterlevel) {
                  if (current.position.z< waterlevel){
                    vertex(current.position.x, current.position.y, waterlevel);
                    vertex(neighbor.position.x, neighbor.position.y, neighbor.position.z);
                  } else {
                    vertex(current.position.x, current.position.y, current.position.z);
                    vertex(neighbor.position.x, neighbor.position.y, waterlevel);
                  }
                  
                } else {
                vertex(current.position.x, current.position.y, current.position.z);
                vertex(neighbor.position.x, neighbor.position.y, neighbor.position.z);
                }
              } else {
                  if(current.position.z< waterlevel && neighbor.position.z < waterlevel){
                  stroke(color(220, 77, 84));
                  vertex(current.position.x, current.position.y, 0);
                  vertex(neighbor.position.x, neighbor.position.y, 0);
                
                } else {
                vertex(current.position.x, current.position.y, 0);
                vertex(neighbor.position.x, neighbor.position.y, 0);
                
              }
              }
          }
        } 
      } 
      endShape();
      if (selectionView){
        selectPoint("peak");
        if (myMap.selectedStart != null){
          translate(myMap.selectedStart.position.x, myMap.selectedStart.position.y);
          noStroke();
          sphere(3);
          translate(-myMap.selectedStart.position.x, -myMap.selectedStart.position.y);
        } else if (myMap.selectedEnd != null){
          translate(myMap.selectedEnd.position.x, myMap.selectedEnd.position.y);
          noStroke();
          sphere(3);
          translate(-myMap.selectedEnd.position.x, -myMap.selectedEnd.position.y);
          engine.search(myMap.selectedStart, myMap.selectedEnd, 'b');
        } else {
          myMap.selectedEnd = null;
          myMap.selectedStart = null;
        } 
       } else {
         if(myMap.selectedStart != null && myMap.selectedEnd != null){
            translate(myMap.selectedStart.position.x, myMap.selectedStart.position.y, myMap.selectedStart.position.z);
            fill(300, 100, 100);
            noStroke();
            sphere(6);
            translate(-myMap.selectedStart.position.x, -myMap.selectedStart.position.y, -myMap.selectedStart.position.z);
            translate(myMap.selectedEnd.position.x, myMap.selectedEnd.position.y, myMap.selectedEnd.position.z);
            sphere(6);
            translate(-myMap.selectedEnd.position.x, -myMap.selectedEnd.position.y, -myMap.selectedEnd.position.z);
           }
       }
    } catch (ConcurrentModificationException e){
    }
   
  }
  
  
  
  public void recursive(PriorityQueue<Vertex> vericesToBeVisited){
    Vertex current = vericesToBeVisited.poll();
    if (current != null){
      for (Vertex neighbor : current.getNextVertecies()){
        if (vericesToBeVisited.contains(neighbor)){
            stroke(color((255 - map(current.edges.get(neighbor), myMap.minHeight, myMap.maxHeight, 20, 150)), 255, 255));
            strokeWeight(1);
            if(neighbor.inSearchPath && current.inSearchPath){
              strokeJoin(ROUND);
              strokeWeight(4);
            }
            beginShape(LINES);
            vertex(current.position.x, current.position.y, current.position.z);
            vertex(neighbor.position.x, neighbor.position.y, neighbor.position.z);
            endShape();
            recursive(vericesToBeVisited); 
            
        }
      }
    }
   
  }
  
  public void changeRenderMode() {
    selectionView = (selectionView == true) ? false : true;
  }
  
  public void selectPoint(String modus) {
    Vertex nearestVertex = null;
    for ( Vertex currentVertex: myMap.vertices) {
      if (nearestVertex == null){
          nearestVertex=currentVertex;
      }
      if (positionIsCloser(new PVector(mouseX, mouseY, 0), nearestVertex.position, currentVertex.position)){
         nearestVertex = currentVertex;
      }
    }
    if (modus == "peak"){
        fill(320, 360, 360);
        ellipse(nearestVertex.position.x, nearestVertex.position.y, 15, 15);
    } else if (modus == "select"){
      if ( myMap.selectedStart!= null && myMap.selectedEnd!= null){
             myMap.selectedStart = null;
            myMap.selectedEnd = null;
      }
      if(nearestVertex!=null){
        if(myMap.selectedStart == null){
            myMap.selectedStart = nearestVertex;
        } else if (myMap.selectedEnd == null){
           myMap.selectedEnd = nearestVertex;
            search(myMap.selectedStart, myMap.selectedEnd, 'v');
            selectionView = false;
        } 
      }
    }
      
  }
  
  
  /*
  public float calcEdge(Vertex a, Vertex b){
    float heightEdge = abs(a.position.z - b.position.z);
    float widthEdge = sqrt(abs(pow(a.position.x - b.position.x, 2) - pow(a.position.y - b.position.y, 2)));
    float hypotenuse = sqrt(pow(heightEdge, 2) + pow(widthEdge, 2));
  
    return hypotenuse;
  }
  */
  public boolean positionIsCloser(PVector origin, PVector oldPosition , PVector newPosition) {
    float distanceOriginToOld = sqrt(pow(abs(origin.x - oldPosition.x), 2) + pow(abs(origin.y - oldPosition.y), 2));
    float distanceOriginToNew = sqrt(pow(abs(origin.x - newPosition.x), 2) + pow(abs(origin.y - newPosition.y), 2));
    return distanceOriginToNew < distanceOriginToOld;
  }
  
  public void drawVertecies(){
  PriorityQueue<Vertex> vertices = myMap.getVertices();
  background(background);
  rotateX(angleX); //60 Grad drehung   
  rotateZ(angleZ);
  translate(-width /2, -height /2);
  
    for (Vertex currentVertex : vertices){
      if (myMap.dimensions <100) {
         noStroke();
         fill(color(map(currentVertex.position.z, 10, 70, 17, 100),200,200));
         if(currentVertex.inSearchPath) fill(highlighting);
         ellipse(currentVertex.position.x, currentVertex.position.y, myMap.gridSize / 2, myMap.gridSize / 2);
       }
       
       drawEdges(currentVertex);
      
        
    }
  }
  
  public void drawEdges(Vertex currentVertex){
     Set<Vertex> nextVertices = currentVertex.getNextVertecies();
       for (Vertex nextVertex: nextVertices){
         stroke(gray);
         if(currentVertex.inSearchPath && currentVertex.nextInSearchPath == nextVertex){
           stroke(highlighting);
         }
         line(currentVertex.position.x, currentVertex.position.y, nextVertex.position.x, nextVertex.position.y);
       
       }
  
  }
  
  public Vertex getVertexOnPosition(int posX, int posY) {
    
    return null;
  }
  
  public void search(Vertex a, Vertex b, char code) {
    deleteSearch();
    if (a != null && b != null){
        SearchAlgorithm aStart = new AStar();
        aStart.search(a, b);
        return;
    }
    Vertex[] verticesArray = new Vertex[0];
    verticesArray = myMap.getVertices().toArray(verticesArray);
    SearchAlgorithm bf;
    if(code == 'v'){
     bf = new AStar();
    } else {
      bf = new BreathFirst();
    }
    bf.search(verticesArray[0], verticesArray[verticesArray.length -1]);
  }
  
  public void deleteSearch(){
    for (Vertex current : myMap.vertices){
      current.inSearchPath = false;
      current.nextInSearchPath = null;
    }
  }
  


}
